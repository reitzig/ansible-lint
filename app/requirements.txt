ansible-lint==6.17.2
ansible==7.7.0
Jinja2==3.1.2
MarkupSafe==2.1.3
ansible-lint-junit==0.17.7
jmespath==1.0.1
netaddr==0.8.0
